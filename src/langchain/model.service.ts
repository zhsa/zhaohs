import { ChatOpenAI } from '@langchain/openai';
import { Injectable } from '@nestjs/common';
import {z} from 'zod'
import {zodToJsonSchema} from 'zod-to-json-schema'
@Injectable()
export class ModelService {
  openAiModel = null;
  constructor() {
    this.initOpenAiModel();
  }
  initModel(options:any) {
    switch (options.type) {
      case 'openai':
        return this.initOpenAiModel();
      default:
        return this.initOpenAiModel();
    }
  }
  initTools(model){
    model.bind({
      tools:[
        {
          type:"function",
          function:{
            name:"weather",
            description:"获取给定位置的天气信息",
            parameters:zodToJsonSchema(z.object({
              location:z.string().describe('城市或地区。例如："上海"')
            }))
          }
        }
      ],
      tool_choice: {
        type: "function",
        function: { name: "weather" },
      },
    })
  }
  initOpenAiModel() {
    if (this.openAiModel) return this.openAiModel;
    this.openAiModel = new ChatOpenAI({
      configuration: {
        baseURL: process.env.baseURL,
      },
      modelName:process.env.model
    });
    this.initTools(this.openAiModel)
    return this.openAiModel;
  }
}
