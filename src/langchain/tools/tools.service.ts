import { Injectable } from '@nestjs/common';
import { ChatOpenAI } from '@langchain/openai';
@Injectable()
export class ToolsService {
  model;
  constructor() {
    this.initModel();
  }
  initModel() {
    if (this.model) return this.model;
    this.model = new ChatOpenAI({
      configuration: {
        baseURL: process.env.baseURL,
      },
      // 讯飞星火
      modelName:"SparkDesk-v3.1"
    });
    return this.model;
  }
}
