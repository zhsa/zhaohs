import { Controller, Get, Inject, Query } from '@nestjs/common';
import { ModelService } from './model.service';
import { InvokeService } from './invoke.service';
@Controller('langchain')
export class LangchainController {
  constructor(private readonly modelService: ModelService) {}
  @Inject(InvokeService)
  private readonly invokeService: InvokeService;
  @Get('send')
  async send(@Query('text') text: string) {
    return await this.invokeService.invoke(text);
  }

}
