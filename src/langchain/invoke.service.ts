import { Inject, Injectable } from '@nestjs/common';
import { ModelService } from './model.service';
import { HumanMessage } from '@langchain/core/messages';
import {StringOutputParser} from '@langchain/core/output_parsers'
@Injectable()
export class InvokeService {
  model = null;
  constructor(private modelService: ModelService) {
    // console.log('this.modelService',this.modelService);
    this.model = this.modelService.initModel({ type: 'openai' });
  }
  async invoke(text: string) {
    if(!text) throw new Error('text is required');
    let outputPrase = new StringOutputParser()
    let model = this.model.pipe(outputPrase)
    console.log(text)
    let res = await model.invoke([new HumanMessage(text)]);
    console.log(res)
    return res
  }
}
