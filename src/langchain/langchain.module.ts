import { Global, Module } from '@nestjs/common';
import { ModelService } from './model.service';
import { LangchainController } from './langchain.controller';
import { InvokeService } from './invoke.service';
import { ToolsService } from './tools/tools.service';
@Global()
@Module({
  controllers: [LangchainController],
  providers: [ModelService, InvokeService, ToolsService],
})
export class LangchainModule {}
