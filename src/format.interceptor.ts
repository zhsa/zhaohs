import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, map } from 'rxjs';

interface FormatData<T> {
  data: T;
  code: number;
  message: string;
}

@Injectable()
export class FormatInterceptor<T> implements NestInterceptor<T, FormatData<T>> {
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<FormatData<T>> {
    return next.handle().pipe(
      map((data) => ({
        data,
        code: 200,
        message: 'success',
      })),
    );
  }
}
