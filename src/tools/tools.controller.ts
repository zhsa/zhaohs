import { Controller, Get, Query } from '@nestjs/common';
import { ToolsService } from './tools.service';

@Controller('tools')
export class ToolsController {
  constructor(private readonly toolsService: ToolsService) {}
  @Get('weather')
  async weather(@Query('location') location: string){
    let res = await this.toolsService.weather(location)
    return res
  }
  
}
