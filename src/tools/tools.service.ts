import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class ToolsService {
  async weather(location:string){
    // 根据地址拿到locationID
    let locations = await axios.get(`https://geoapi.qweather.com/v2/city/lookup`,{
      params:{
        location:location,
        key:process.env.HEFENG_API_KEY,
        range:'cn'
      }
    })
    let locationID = locations.data?.location[0]?.id
    if(!locationID) throw new Error('未找到该地址')
    let res = await axios.get(`https://devapi.qweather.com/v7/weather/now`,{
      params:{
        location:locationID,
        key:process.env.HEFENG_API_KEY
      }
    })
    return res.data.now
  }
}
